# this project was forked by _*[cha/nodetestplat](https://lab.er.co.th/cha/nodetestplat)*_ project

## docker-image dependencies graph

```text
+-- alpine:3.8 (3.962 MB)                              ====> official image
    |
    +-- node:10.7.0-alpine (64.64 MB)                  ====> official image
    |   |
    |   `-- ops/ng:6.1.0-alpine (108 MB)               ====> for ng dev build or production build
    |       |
    |      (+)-- ops/ng-testplat:6.1.0-alpine (285 MB) ====> for ng e2e and unit testing
    |       |
    `-------'--- ops/e2e-testplat:64-alpine (181 MB)   ====> this project
```

| version   | alpine | ttf-freefont | eudev    | dbus / -x11 | xvfb      | chromium / -chromedriver | firefox-esr |
| --------- | ------ | ------------ | -------- | ----------- | --------- | ------------------------ | ----------- |
| 64-alpine |  3.8   | n/a          | 3.2.5-r2 | 1.10.24-r1  | 1.19.6-r2 | 64.0.3282.168-r0         | 52.8.1-r0   |
| 61-alpine |  3.7   | n/a          | 3.2.4-r1 | 1.10.24-r0  | 1.19.5-r0 | 61.0.3163.100-r0         | 52.7.3-r0   |
| 57-alpine |  3.6   | n/a          | 3.2.2-r1 | 1.10.14-r0  | 1.19.3-r2 | 57.0.2987.133-r0         | 52.5.0-r0   |

## e2e-testplat alpine packages

- ttf-freefont
- eudev=3.2.5-r2
- dbus=1.10.24-r1
- dbus-x11=1.10.24-r1
- xvfb=1.19.6-r2
- chromium=64.0.3282.168-r0
- chromium-chromedriver=64.0.3282.168-r0
- firefox-esr=52.8.1-r0
