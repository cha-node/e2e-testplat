FROM alpine:3.8

LABEL maintainer="Anucha Nualsi <ana.cpe9@gmail.com>"

# install chrome && firefox
# http://engineering.pivotal.io/post/headless-ui-testing-with-go-agouti-and-chrome/
# /etc/init.d/dbus has the wrong location
#RUN apk add --update-cache "firefox=50.0-r0" --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing/ --allow-untrusted
COPY entrypoint.cha.sh chrome-fix.sh /

ENV DISPLAY=:99 CHROME_BIN=/chrome-fix.sh FIREFOX_BIN=/usr/bin/firefox LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 LANGUAGE=en_US.UTF-8

VOLUME /.npm/

RUN chmod a+x /entrypoint.cha.sh && \
    chmod a+x /chrome-fix.sh && \
    apk update && \
    apk upgrade && \
    apk add --no-cache --update \
    ttf-freefont \
    "eudev=3.2.5-r2" \
    "dbus=1.10.24-r1" \
    "dbus-x11=1.10.24-r1" \
    "xvfb=1.19.6-r2" \
    "chromium=68.0.3440.75-r0" \
    "chromium-chromedriver=68.0.3440.75-r0" \
    "firefox-esr=52.9.0-r0" && \
    ln -sf /usr/bin/dbus-daemon /bin/dbus-daemon && \
    ln -sf /usr/bin/dbus-uuidgen /bin/dbus-uuidgen && \
    ln -sf /usr/bin/dbus-binding-tool /dbus-binding-tool && \
    ln -sf /usr/bin/dbus-cleanup-sockets /dbus-cleanup-sockets && \
    ln -sf /usr/bin/dbus-monitor /dbus-monitor && \
    ln -sf /usr/bin/dbus-run-session /bin/dbus-run-session && \
    ln -sf /usr/bin/dbus-send /bin/dbus-send && \
    ln -sf /usr/bin/dbus-test-tool /bin/dbus-test-tool && \
    ln -sf /usr/bin/dbus-update-activation-environment /bin/dbus-update-activation-environment && \
    rm -rf /tmp/* && \
    rm -rf /var/tmp/* && \
    rm -rf /var/cache/apk/*

ENTRYPOINT ["/entrypoint.cha.sh"]